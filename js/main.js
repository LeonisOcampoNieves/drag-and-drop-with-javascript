// Declaramos cual va ser nuestra lista
const lista = document.getElementById('lista')

// vamos a decirle que esa lista va a trabajar con Sortable
Sortable.create(lista, {
    animation: 150,
    
    /* 
        Nos permite establecer el nombre de clase que vamos a querer que tenga,
        para que cuando seleccionemos un elemento y dejemos apretado el cursor,
        cambie su clase
    */
    chosenClass: "seleccionado",
    //ghostClass: "fantasma",
    dragClass: "drag",

    // Nos permite ejecutar el código que uno quiera cuando el usuario suelte lo que esté arrastrando
    onEnd: () => {
        console.log('Se insertó un elemento')
    },
    group: "lista-personas",
    store: {
        // Guardamos el orden de la lista
        set: (sortable) => {
            // toArray nos premite transformar cada uno de los elementos de la lista en un arreglo
            const orden = sortable.toArray()
            
            /*
                join nos permite tomar cada uno de los elementos y pasarlos a una cadena de texto
                y separarlos por un elemento, ya que localStorage solo permite guardar cadenas de texto.

                con sortable.options.gropu.name obtenemos el nombre del grupo
            */
            localStorage.setItem(sortable.options.group.name, orden.join('|'))
        },

        // Obtenemos el orden de la lista
        get: (sortable) => {
            const orden = localStorage.getItem(sortable.options.group.name)

            // Convertimos a elementos de arreglo
            return orden ? orden.split('|') : []
        }
    }
}); // recibe 2 parametros; la lista y un objeto de las opciones que tenga